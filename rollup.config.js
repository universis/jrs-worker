import typescript from '@rollup/plugin-typescript';
import * as pkg from './package.json';
const externals = Object.keys(pkg.dependencies);
if (pkg.peerDependencies) {
    externals.push.apply(externals, Object.keys(pkg.peerDependencies));
}
export default [
    {
        input: 'src/index.ts',
        output: {
            dir: 'dist',
            format: 'cjs',
            sourcemap: true
        },
        external: externals,
        plugins: [typescript()]
    },
    {
        input: 'src/index.ts',
        output: {
            file: 'dist/index.esm.js',
            format: 'esm',
            sourcemap: true
        },
        external: externals,
        plugins: [typescript()]
    }
];