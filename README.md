# @universis/jrs-worker

JasperReports Server Worker for Universis Platform

## Getting started

Execute `@universis/jrs-worker`

    npx @universis/jrs-worker [--port <port>] [--host <host>] [--config <config>] [--target <target>] [--help]

where `--port` is the port number, `--host` is the remote address on which server will listen and `--config` is the path where the configuration file of the service is located.
Default port is `8088`, default host is `127.0.0.1` and default config is located at `config/app.production.json`.

Use `--target` to set target URL of JasperReports Server e.g.

    npx @universis/jrs-worker --target "http://localhost:8080/jasperserver/rest_v2/"

## Configuration

Create `config/app.production.json` file

```json
{
    "settings": {
        "jrsWorker": {
            "target": "http://localhost:8080/jasperserver/rest_v2/",
            "workerPool": {
            }
        }
    }
}
```
where jrsWorker.target is URL of JasperReports Server and jrsWorker.workerPool is [generic-pool](https://github.com/coopernurse/node-pool?tab=readme-ov-file#generic-pool) configuration.


`@universis/jrs-worker` acts as proxy for JasperReports Server by limiting number of concurrent requests to JasperReports Server. It can be used as standalone server by using and configuring [PM2](https://pm2.keymetrics.io/docs/usage/quick-start/) process manager.

## Install as PM2 service

Install `@universal/jrs-worker` globally

    npm install -g @universis/jrs-worker

Execute `pm2 start` command

    pm2 start jrs-worker --name jrs-worker -- --target "http://localhost:8080/jasperserver/rest_v2/"

Check status of the service by running `pm2 list` command

    pm2 list

```
┌────┬───────────────┬─────────────┬─────────┬─────────┬──────────┬────────┬──────┬───────────┬──────────┬──────────┬──────────┬──────────┐
│ id │ name          │ namespace   │ version │ mode    │ pid      │ uptime │ ↺    │ status    │ cpu      │ mem      │ user     │ watching │
├────┼───────────────┼─────────────┼─────────┼─────────┼──────────┼────────┼──────┼───────────┼──────────┼──────────┼──────────┼──────────┤
│ 0  │ jrs-worker    │ default     │ 0.39.0  │ fork    │ 10000    │  1s    │ 0    │ online    │ 0%       │ 51.0mb   │ root     │ disabled │
└────┴───────────────┴─────────────┴─────────┴─────────┴──────────┴────────┴──────┴───────────┴──────────┴──────────┴──────────┴──────────┘
```