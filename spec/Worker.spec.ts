import express, { Application } from 'express';
import request from 'supertest';

describe('JrsWorker', () => {

    let app: Application;
    beforeAll((done) => {
        app = express();
        return done();
    });
    it('should use worker', async () => {
        const response = await request(app)
            .get('/');
        expect(response.status).toBe(200);
    });

});